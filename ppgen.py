#!/usr/bin/env python

import codecs
import random
import sys
import unicodedata


DICTIONARY = "/usr/share/dict/words"

LEN_PROBA = {  # length --> proba
    2: 0,
    3: 0,
    4: 1,
    5: 2,
    6: 2,
}

PADDING = [
    '!', '-', '.', '#', '+', '_', '0', '1', '2', '3', '4', '5', '6', '7', '8',
    '9'
]
PADDING_LENGTH = 2

CASE_PROBA = {  # proba --> func
    3: unicode.lower,
    1: unicode.title,
}


def load_dict():
    dictionary = {}  # length --> words
    with codecs.open(DICTIONARY, 'r', encoding='utf-8') as file_desc:
        for word in file_desc:
            word = word.strip()
            lword = len(word)
            if lword in dictionary:
                dictionary[lword].append(word)
            else:
                dictionary[lword] = [word]
    return dictionary


def get_word_length():
    sum_proba = sum(LEN_PROBA.values())
    proba = random.randint(0, sum_proba - 1)
    for (length, len_proba) in LEN_PROBA.iteritems():
        proba -= len_proba
        if proba <= 0:
            return length
    assert()


def find_word(dictionary, length):
    words = dictionary[length]
    return words[random.randint(0, len(words) - 1)]


def strip_accents(string):
    """
    Strip all the accents from the string
    """
    return u''.join(
        (character for character in unicodedata.normalize('NFD', string)
         if unicodedata.category(character) != 'Mn'))


def change_case(word):
    word = word.lower()
    sum_proba = sum(CASE_PROBA.keys())
    proba = random.randint(0, sum_proba - 1)
    for (fproba, func) in CASE_PROBA.iteritems():
        proba -= fproba
        if proba < 0:
            return func(word)
    assert()


def add_padding(passphrase, padding_length):
    for _ in xrange(0, padding_length):
        char = PADDING[random.randint(0, len(PADDING) - 1)]
        position = random.randint(0, len(passphrase))
        passphrase.insert(position, char)
    return passphrase


def main():
    wanted_length = int(sys.argv[1])
    wanted_length -= PADDING_LENGTH

    dictionary = load_dict()

    for a in xrange(0, 12):
        passphrases = []
        for b in xrange(0, 4):
            passphrase = []
            while sum(len(x) for x in passphrase) < wanted_length:
                length = get_word_length()
                if length > wanted_length:
                    length = wanted_length
                word = find_word(dictionary, length)
                word = word.strip()
                word = strip_accents(word)
                word = change_case(word)
                passphrase.append(word)
            passphrase = add_padding(
                passphrase, PADDING_LENGTH)
            passphrase = u"".join(passphrase)
            passphrases.append(passphrase)
        print(u"\t".join(passphrases))


if __name__ == '__main__':
    main()
